import React, { Component } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {
  AddNewMemo as AddNewMemoAction,
  RemoveMemo as RemoveMemoAction,
  UpdateCheck as UpdateCheckAction,
} from '../../actions/app_start';
import MemoListItem from '../components/MemoListItem';

const styles = StyleSheet.create({
  itemSeparator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#808080',
  },
  memoItemContainer: {
    width: Dimensions.get('window').width,
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
});

class MemosListScreen2 extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.memo.name,
  });

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  processMemos = (memos) => {
    memos.sort((a, b) => (b.createdDate - a.createdDate));
    const memosResult = [];
    const { text } = this.state;
    memosResult.push(
      <View
        key={-1}
        style={styles.memoItemContainer}
      >
        <TextInput
          style={{ flex: 1 }}
          placeholder="Type new memo here..."
          onChangeText={this.onChangeText}
          maxLength={30}
          underlineColorAndroid="transparent"
          value={text}
        />
        <TouchableOpacity
          onPress={this.addNewMemo}
        >
          <MaterialCommunityIcons
            name="library-plus"
            size={32}
            color={text.trim() !== '' ? '#1a651a' : '#808080'}
          />
        </TouchableOpacity>
      </View>,
    );
    memos.forEach((memo) => {
      memosResult.push(
        <MemoListItem
          key={memo.id}
          memo={memo}
          processMemoCheck={this.processMemoCheck}
          onLongPress={() => {
            Alert.alert(
              'Remove Memo',
              `Are you sure you wish to remove ${memo.name}?`,
              [
                { text: 'Don\'t remove', onPress: () => {} },
                {
                  text: 'Yes',
                  onPress: () => {
                    const { navigation, RemoveMemo } = this.props;
                    RemoveMemo(navigation.state.params.memo.id, memo);
                  },
                },
              ],
            );
          }}
        />,
      );
    });
    return memosResult;
  };

  onChangeText = (text) => {
    this.setState({
      text,
    });
  };

  processMemoCheck = (memo) => {
    const { navigation, UpdateCheck } = this.props;
    UpdateCheck(navigation.state.params.memo.id, memo);
  };

  addNewMemo = () => {
    const { text } = this.state;
    if (text.trim().length > 0) {
      const name = text.trim();
      const { navigation, AddNewMemo } = this.props;
      AddNewMemo(navigation.state.params.memo.id, name);
      this.state.text = '';
    }
  };

  render() {
    const { appStart, navigation } = this.props;
    const memo = appStart.memos.filter(
      memoo => memoo.id === navigation.state.params.memo.id,
    )[0];
    const memos = this.processMemos(memo.memos);
    return (
      <FlatList
        keyboardShouldPersistTaps="always"
        data={memos}
        renderItem={({ item }) => item}
        ItemSeparatorComponent={() => (
          <View style={styles.itemSeparator} />
        )}
      />
    );
  }
}

MemosListScreen2.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  appStart: PropTypes.shape({}),
  AddNewMemo: PropTypes.func,
  RemoveMemo: PropTypes.func,
  UpdateCheck: PropTypes.func,
};

MemosListScreen2.defaultProps = {
  appStart: {},
  AddNewMemo: () => {},
  RemoveMemo: () => {},
  UpdateCheck: () => {},
};

const mapStateToProps = state => ({
  appStart: state.appStart,
});

export default connect(
  mapStateToProps,
  {
    AddNewMemo: AddNewMemoAction,
    RemoveMemo: RemoveMemoAction,
    UpdateCheck: UpdateCheckAction,
  },
)(MemosListScreen2);
