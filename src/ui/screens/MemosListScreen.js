import React from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  FlatList,
  Keyboard,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {
  AddNewMemoFolder as AddNewMemoFolderAction,
  LoadMemos as LoadMemosAction,
  RemoveMemoFolder as RemoveMemoFolderAction,
} from '../../actions/app_start';
import MemoFolderListItem from '../components/MemoFolderListItem';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  itemSeparator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#808080',
  },
  memoItemContainer: {
    width: Dimensions.get('window').width,
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
});

class MemosListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  processMemos = (memos) => {
    memos.sort((a, b) => (b.createdDate - a.createdDate));
    const memosResult = [];
    const { text } = this.state;
    memosResult.push(
      <View
        key={-1}
        style={styles.memoItemContainer}
      >
        <TextInput
          style={{ flex: 1 }}
          placeholder="Type new topic name here..."
          onChangeText={this.onChangeText}
          maxLength={30}
          underlineColorAndroid="transparent"
          value={text}
        />
        <TouchableOpacity
          onPress={this.addNewMemo}
        >
          <MaterialCommunityIcons
            name="library-plus"
            size={32}
            color={text.trim() !== '' ? '#1a651a' : '#808080'}
          />
        </TouchableOpacity>
      </View>,
    );
    const { navigation } = this.props;
    memos.forEach((memo) => {
      memosResult.push(
        <MemoFolderListItem
          key={memo.id}
          memo={memo}
          onPress={() => {
            navigation.navigate('MemosListScreen2', {
              memo,
            });
          }}
          onLongPress={() => {
            Alert.alert(
              'Remove Memo',
              `Are you sure you wish to remove ${memo.name}?`,
              [
                { text: 'Don\'t remove', onPress: () => {} },
                {
                  text: 'Yes',
                  onPress: () => {
                    const { RemoveMemoFolder } = this.props;
                    RemoveMemoFolder(memo.id);
                  },
                },
              ],
            );
          }}
        />,
      );
    });
    return memosResult;
  };

  onChangeText = (text) => {
    this.setState({
      text,
    });
  };

  addNewMemo = () => {
    const { text } = this.state;
    if (text.trim().length > 0) {
      const name = text.trim();
      const { AddNewMemoFolder } = this.props;
      AddNewMemoFolder(name);
      this.setState({
        text: '',
      });
      Keyboard.dismiss();
    }
  };

  render() {
    const { appStart } = this.props;
    if (appStart.message) {
      Alert.alert(appStart.message);
    }
    if (appStart.loading) {
      const { LoadMemos } = this.props;
      LoadMemos();
      return (
        <View style={styles.container}>
          <ActivityIndicator />
        </View>
      );
    }
    const memos = this.processMemos(appStart.memos);
    return (
      <FlatList
        keyboardShouldPersistTaps="always"
        data={memos}
        renderItem={({ item }) => item}
        ItemSeparatorComponent={() => (
          <View style={styles.itemSeparator} />
        )}
      />
    );
  }
}

MemosListScreen.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
  appStart: PropTypes.shape({}),
  AddNewMemoFolder: PropTypes.func,
  LoadMemos: PropTypes.func,
  RemoveMemoFolder: PropTypes.func,
};

MemosListScreen.defaultProps = {
  appStart: {},
  AddNewMemoFolder: () => {},
  LoadMemos: () => {},
  RemoveMemoFolder: () => {},
};

const mapStateToProps = state => ({
  appStart: state.appStart,
});

export default connect(
  mapStateToProps,
  {
    AddNewMemoFolder: AddNewMemoFolderAction,
    LoadMemos: LoadMemosAction,
    RemoveMemoFolder: RemoveMemoFolderAction,
  },
)(MemosListScreen);
