import React from 'react';
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { PropTypes } from 'prop-types';
import MemoAppCheckIcon from './MemoAppCheckIcon';
import { PrettyTimeFromMs } from '../../utils';

const MemoListItem = ({
  memo,
  processMemoCheck,
  onPress,
  onLongPress,
}) => (
  <TouchableOpacity
    onPress={onPress}
    onLongPress={onLongPress}
  >
    <View
      style={{
        width: Dimensions.get('window').width,
        height: 64,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 16,
        paddingRight: 16,
      }}
    >
      <View
        style={{
          flexDirection: 'column',
          justifyConent: 'center',
        }}
      >
        <Text
          style={{
            textDecorationLine: memo.check ? 'line-through' : 'none',
          }}
        >
          {memo.name}
        </Text>
        <Text
          style={{
            color: '#808080',
          }}
        >
          {PrettyTimeFromMs(memo.createdDate)}
        </Text>
      </View>
      <MemoAppCheckIcon
        checked={memo.check}
        onPress={() => processMemoCheck(memo)}
      />
    </View>
  </TouchableOpacity>
);

MemoListItem.propTypes = {
  memo: PropTypes.shape({}),
  processMemoCheck: PropTypes.func,
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
};

MemoListItem.defaultProps = {
  memo: {},
  processMemoCheck: () => {},
  onPress: () => {},
  onLongPress: () => {},
};

export default MemoListItem;
