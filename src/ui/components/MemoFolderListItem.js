import React from 'react';
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { PropTypes } from 'prop-types';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { PrettyTimeFromMs } from '../../utils';

const MemoFolderListItem = ({
  memo,
  onPress,
  onLongPress,
}) => (
  <TouchableOpacity
    onPress={onPress}
    onLongPress={onLongPress}
  >
    <View
      style={{
        width: Dimensions.get('window').width,
        height: 64,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 16,
        paddingRight: 16,
      }}
    >
      <View
        style={{
          flexDirection: 'column',
          justifyConent: 'center',
        }}
      >
        <Text
          style={{
            textDecorationLine: memo.memos.length > 0
              && memo.memos.filter(memoo => !memoo.check).length === 0 ? 'line-through' : 'none',
          }}
        >
          {memo.name}
        </Text>
        <Text
          style={{
            color: '#808080',
          }}
        >
          {PrettyTimeFromMs(memo.createdDate)}
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyConent: 'center',
        }}
      >
        <View
          style={{
            marginRight: 8,
            height: 32,
            width: 32,
            backgroundColor: '#1a651a',
            borderRadius: 16,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text
            style={{
              color: '#ffffff',
            }}
          >
            {memo.memos.filter(memoo => !memoo.check).length}
          </Text>
        </View>
        <MaterialCommunityIcons
          name="arrow-right"
          size={32}
          color="#1a651a"
        />
      </View>
    </View>
  </TouchableOpacity>
);

MemoFolderListItem.propTypes = {
  memo: PropTypes.shape({}),
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
};

MemoFolderListItem.defaultProps = {
  memo: {},
  onPress: () => {},
  onLongPress: () => {},
};

export default MemoFolderListItem;
