import React from 'react';
import { PropTypes } from 'prop-types';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const MemoAppCheckIcon = ({
  checked,
  onPress,
}) => (
  <MaterialCommunityIcons
    name={checked ? 'checkbox-marked-outline' : 'checkbox-blank-outline'}
    size={35}
    color={checked ? '#1a651a' : '#808080'}
    onPress={onPress}
  />
);

MemoAppCheckIcon.propTypes = {
  checked: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
};

MemoAppCheckIcon.defaultProps = {
  onPress: () => {},
};

export default MemoAppCheckIcon;
