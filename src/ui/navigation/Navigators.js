import { createAppContainer } from 'react-navigation';
import AppStackNav from './AppStack';

const AppStack = createAppContainer(AppStackNav);

// eslint-disable-next-line import/prefer-default-export
export { AppStack };
