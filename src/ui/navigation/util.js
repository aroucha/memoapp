// eslint-disable-next-line import/prefer-default-export
export function getActiveRouteNameAndParams(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteNameAndParams(route);
  }
  return {
    routeName: route.routeName,
    params: route.params,
  };
}
