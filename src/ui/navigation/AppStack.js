import { createStackNavigator } from 'react-navigation';
import MemosListScreen from '../screens/MemosListScreen';
import MemosListScreen2 from '../screens/MemosListScreen2';


export default createStackNavigator({
  MemosListScreen: {
    screen: MemosListScreen,
    navigationOptions: {
      title: 'Memos',
    },
  },
  MemosListScreen2: {
    screen: MemosListScreen2,
  },
},
{
  headerMode: 'float',
  defaultNavigationOptions: {
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#1a651a',
    },
  },
});
