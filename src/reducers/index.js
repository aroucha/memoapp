import { combineReducers } from 'redux';
import AppStartReducer from './app_start';

const RootReducer = combineReducers({
  appStart: AppStartReducer,
});

export default RootReducer;
