const initialState = {
  loading: true,
  memos: [],
  message: false,
};

export default function (state = initialState, action) {
  let newObject;
  switch (action.type) {
    case 'ERROR':
      console.log(`unhandled error: ${action.payload.error}`);
      return Object.assign({}, state, {
        loading: false,
        message: action.payload.error,
      });

    case 'MEMOS_LOADED':
      return Object.assign({}, state, {
        loading: false,
        memos: action.payload.memos,
        message: false,
      });

    case 'NEW_MEMO_FOLDER':
      return Object.assign({}, state, {
        loading: false,
        memos: [
          ...state.memos,
          action.payload.memo,
        ],
        message: false,
      });

    case 'REMOVE_MEMO_FOLDER':
      return Object.assign({}, state, {
        loading: false,
        memos: [
          ...state.memos.filter(memo => memo.id !== action.payload.memoId),
        ],
        message: false,
      });

    case 'NEW_MEMO':
      newObject = Object.assign(
        state.memos.filter(memo => memo.id === action.payload.memoId),
      )[0];
      if (!newObject) {
        return state;
      }
      newObject.memos.push(action.payload.memo);
      return Object.assign({}, state, {
        loading: false,
        memos: [
          ...state.memos.filter(memo => memo.id !== action.payload.memoId),
          newObject,
        ],
        message: false,
      });

    case 'UPDATE_CHECK':
      newObject = Object.assign(
        state.memos.filter(memo => memo.id === action.payload.memoId),
      )[0];
      if (!newObject) {
        return state;
      }
      for (let i = 0; i < newObject.memos.length; i += 1) {
        if (newObject.memos[i].id === action.payload.id) {
          newObject.memos[i].check = action.payload.check;
          break;
        }
      }
      return Object.assign({}, state, {
        loading: false,
        memos: [
          ...state.memos.filter(memo => memo.id !== action.payload.memoId),
          newObject,
        ],
        message: false,
      });

    case 'REMOVE_MEMO':
      newObject = Object.assign(
        state.memos.filter(memo => memo.id === action.payload.memoId),
      )[0];
      if (!newObject) {
        return state;
      }
      let i = 0;
      while (i < newObject.memos.length) {
        if (newObject.memos[i].id === action.payload.id) {
          break;
        }
        i += 1;
      }
      newObject.memos.splice(i, 1);
      return Object.assign({}, state, {
        loading: false,
        memos: [
          ...state.memos.filter(memo => memo.id !== action.payload.memoId),
          newObject,
        ],
        message: false,
      });

    default:
      return state;
  }
}
