// eslint-disable-next-line import/prefer-default-export
export function PrettyTimeFromMs(msToPresent) {
  const ms = new Date().getTime() - msToPresent;
  const secs = ms / 1000;
  const mins = secs / 60;
  const hours = mins / 60;
  const days = hours / 24;
  let result = '';
  if (Math.floor(days) > 0) {
    result += Math.floor(days) + ((days > 2) ? ' days' : ' day');
  }
  if (Math.floor(days) < 2) {
    if (Math.floor(hours) > 0) {
      result += ` ${(Math.floor(hours) - (Math.floor(days) * 24))}h`;
    }
    if (Math.floor(days) === 0 && Math.floor(hours) < 3) {
      result += ` ${(Math.floor(mins) - (Math.floor(hours) * 60) - (Math.floor(days) * 24))}min`;
    }
  }
  return `${result} ago`;
}
