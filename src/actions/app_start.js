/* eslint-disable no-underscore-dangle */
import v4 from 'uuid/v4';
import { db } from '../db/db';

export function LoadMemos() {
  return (dispatch) => {
    db.transaction((tx) => {
      tx.executeSql(
        'select * from memos;',
        [],
        (_, { rows }) => {
          const result = [];
          for (let i = 0; i < rows.length; i += 1) {
            // rows._array[i].memos = JSON.parse(rows._array[i].memos);
            // result.push(rows._array[i]);
            result.push({
              ...rows._array[i],
              memos: JSON.parse(rows._array[i].memos),
            });
          }
          dispatch({
            type: 'MEMOS_LOADED',
            payload: {
              memos: result,
            },
          });
        },
        (_, error) => {
          dispatch({
            type: 'ERROR',
            payload: {
              error,
            },
          });
        },
      );
    });
  };
}

export function AddNewMemoFolder(name) {
  return (dispatch) => {
    const createdDate = new Date().getTime();
    db.transaction((tx) => {
      tx.executeSql(
        'insert into memos (name, createdDate, memos) values (?, ?, ?)',
        [name, createdDate, '[]'],
        (_, resultSet) => {
          tx.executeSql(
            'select * from memos where id = ?',
            [resultSet.insertId],
            (__, { rows }) => {
              const memo = rows._array[0];
              memo.memos = JSON.parse(memo.memos);
              dispatch({
                type: 'NEW_MEMO_FOLDER',
                payload: {
                  memo,
                },
              });
            },
            (__, error) => {
              dispatch({
                type: 'ERROR',
                payload: {
                  error,
                },
              });
            },
          );
        },
        (_, error) => {
          dispatch({
            type: 'ERROR',
            payload: {
              error,
            },
          });
        },
      );
    });
  };
}

export function RemoveMemoFolder(memoId) {
  return (dispatch) => {
    db.transaction((tx) => {
      tx.executeSql(
        'delete from memos where id = ?',
        [memoId],
        () => {
          dispatch({
            type: 'REMOVE_MEMO_FOLDER',
            payload: {
              memoId,
            },
          });
        },
        (_, error) => {
          dispatch({
            type: 'ERROR',
            payload: {
              error,
            },
          });
        },
      );
    });
  };
}

export function AddNewMemo(memoId, name) {
  return (dispatch) => {
    const newMemo = {
      id: v4(),
      name,
      check: false,
      createdDate: new Date().getTime(),
    };
    db.transaction((tx) => {
      tx.executeSql(
        'select * from memos where id = ?',
        [memoId],
        (_, { rows }) => {
          const memo = rows._array[0];
          memo.memos = JSON.parse(memo.memos);
          memo.memos.push(newMemo);
          tx.executeSql(
            'update memos set memos = ? where id = ?',
            [JSON.stringify(memo.memos), memoId],
            () => {
              dispatch({
                type: 'NEW_MEMO',
                payload: {
                  memoId,
                  memo: newMemo,
                },
              });
            },
            (__, error) => {
              dispatch({
                type: 'ERROR',
                payload: {
                  error,
                },
              });
            },
          );
        },
        (_, error) => {
          dispatch({
            type: 'ERROR',
            payload: {
              error,
            },
          });
        },
      );
    });
  };
}

export function UpdateCheck(memoId, memo) {
  return (dispatch) => {
    db.transaction((tx) => {
      tx.executeSql(
        'select * from memos where id = ?',
        [memoId],
        (_, { rows }) => {
          const memoFolder = rows._array[0];
          memoFolder.memos = JSON.parse(memoFolder.memos);
          for (let i = 0; i < memoFolder.memos.length; i += 1) {
            if (memoFolder.memos[i].id === memo.id) {
              memoFolder.memos[i].check = !memo.check;
              break;
            }
          }
          tx.executeSql(
            'update memos set memos = ? where id = ?',
            [JSON.stringify(memoFolder.memos), memoId],
            () => {
              dispatch({
                type: 'UPDATE_CHECK',
                payload: {
                  memoId,
                  id: memo.id,
                  check: !memo.check,
                },
              });
            },
            (__, error) => {
              dispatch({
                type: 'ERROR',
                payload: {
                  error,
                },
              });
            },
          );
        },
        (_, error) => {
          dispatch({
            type: 'ERROR',
            payload: {
              error,
            },
          });
        },
      );
    });
  };
}

export function RemoveMemo(memoId, memo) {
  return (dispatch) => {
    db.transaction((tx) => {
      tx.executeSql(
        'select * from memos where id = ?',
        [memoId],
        (_, { rows }) => {
          const memoFolder = rows._array[0];
          memoFolder.memos = JSON.parse(memoFolder.memos);
          let i = 0;
          while (i < memoFolder.memos.length) {
            if (memoFolder.memos[i].id === memo.id) {
              break;
            }
            i += 1;
          }
          memoFolder.memos.splice(i, 1);
          tx.executeSql(
            'update memos set memos = ? where id = ?',
            [JSON.stringify(memoFolder.memos), memoId],
            () => {
              dispatch({
                type: 'REMOVE_MEMO',
                payload: {
                  memoId,
                  id: memo.id,
                },
              });
            },
            (__, error) => {
              dispatch({
                type: 'ERROR',
                payload: {
                  error,
                },
              });
            },
          );
        },
        (_, error) => {
          dispatch({
            type: 'ERROR',
            payload: {
              error,
            },
          });
        },
      );
    });
  };
}
