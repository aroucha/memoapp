import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { Provider } from 'react-redux';
import Store from './src/Store';
import { db } from './src/db/db';
import { AppStack } from './src/ui/navigation/Navigators';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      created: false,
    };
  }

  componentDidMount() {
    db.transaction((tx) => {
      tx.executeSql(
        'create table if not exists memos (id integer primary key not null, name text, createdDate text, memos text);',
        [],
        () => {
          // console.log('success');
          this.setState({
            created: true,
          });
        },
        (_, error) => {
          console.warn(error);
          this.setState({
            created: true,
          });
        },
      );
    });
  }

  render() {
    const { created } = this.state;
    if (!created) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}
        >
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <Provider store={Store}>
        <AppStack />
      </Provider>
    );
  }
}
