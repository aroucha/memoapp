Sample test project using [Create React Native App](https://github.com/react-community/create-react-native-app).

* React Native
* Expo
* Redux
* SQLite

The app is a simple TODOs organizer. On the first screen the user can view, create or remove topics (or directories).
For each topic, the user may navigate to a screen where he can create, remove or (un)check TODOs items.

Run the app:

* clone repo
* on project directory, run npm install
* run on physical device:
  * on project directory, run npm start
  * on device, load app using Expo application
  
[Android Play Store](https://play.google.com/store/apps/details?id=com.aroucha.memoapp)